from django.db import models


class Service(models.Model):
    name = models.CharField(max_length=200)
    text = models.TextField()
    cost = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.name


class Application(models.Model):
    name = models.CharField(max_length=200, blank=True)
    mail = models.CharField(max_length=200, blank=True)
    service = models.CharField(max_length=200, blank=True)
    comment = models.TextField(blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.name
