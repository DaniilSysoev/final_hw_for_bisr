from django.shortcuts import render
from .models import Service
from .forms import ApplicationForm


def index(request):
    if request.method == "POST":
        form = ApplicationForm(request.POST)
        if form.is_valid():
            form.save()
    
    form = ApplicationForm()
    service = Service.objects.all()
    return render(request, 'shrek/index.html', {'service': service, 'form': form})