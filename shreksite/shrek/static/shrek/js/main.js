﻿function Select(c) {
    const images = document.querySelector('.portfolio__example').querySelectorAll('img')
    if (c == 'all') {
        images[0].className = 'portfolio__example__big smile'
        images[1].className = 'portfolio__example__small smile'
        images[2].className = 'portfolio__example__small what'
        images[3].className = 'portfolio__example__big bad'
        images[4].className = 'portfolio__example__small smile'
        images[5].className = 'portfolio__example__small bad'
    } else{
        for (var i = 0; i< images.length; i++) {
            if (images[i].className.indexOf(c) == -1) {
                images[i].classList.add('hidden_img')
            } else {
                console.log(images[i])
                images[i].classList.remove('hidden_img')
                images[i].classList.remove('portfolio__example__small')
                images[i].classList.add('portfolio__example__big')
            }
        }
    }
}

window.addEventListener('DOMContentLoaded', () => {
    const navigation = document.querySelectorAll('.portfolio__navigation__button')
    for (var i = 0; i < navigation.length; i++) {
        navigation[i].addEventListener('click', (event) => {
            if (event.target.className.indexOf('active') === -1) {
                event.target.classList.toggle('active')
                for (var j = 0; j < navigation.length; j++) {
                    if (navigation[j] != event.target) {
                        navigation[j].classList.remove('active')
                    }
                }

            }
        })
    }

    const result_button = document.querySelector('.cost__block_form__button')
    result_button.addEventListener('click', (event) => {
        if (document.querySelector(".cost__block_form__areas__check__box").checked) {
            document.querySelector('.cost__block_result').style= 'display: flex'
        } else {
            alert("Вы не отметили галочку")
        }
    })

    const qa = document.querySelectorAll('.qa__container__big_example')
    for (var i = 0; i < qa.length; i ++) {
        qa[i].addEventListener('click', (event) => {
            event.target.querySelector('img').classList.toggle('qa__rotate')
            event.target.querySelector('.qa__container__big_example__main_text').classList.toggle('qa_open')
        })
    }
    const qa_title = document.querySelectorAll('.qa__container__big_example__title')
    for (var i = 0; i < qa_title.length; i++) {
        qa_title[i].addEventListener('click', (event) => {
            event.target.parentNode.querySelector('img').classList.toggle('qa_rotate')
            event.target.parentNode.querySelector('.qa__container__big_example__main_text').classList.toggle('qa_open')
        })
    }
})