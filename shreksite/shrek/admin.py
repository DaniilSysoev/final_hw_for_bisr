from django.contrib import admin
from .models import Service, Application


admin.site.register(Service)
admin.site.register(Application)